package systemcontracts

import "github.com/ledgerwatch/erigon/common"

var (
	// genesis contracts
	ValidatorContract          = common.HexToAddress("0x0000000000000000000000000000000000001000")
	SlashContract              = common.HexToAddress("0x0000000000000000000000000000000000001001")
	SystemRewardContract       = common.HexToAddress("0x0000000000000000000000000000000000001002")
	GovHubContract             = common.HexToAddress("0x0000000000000000000000000000000000001003")
	StakingContract            = common.HexToAddress("0x0000000000000000000000000000000000001004")
)
